package com.practicatecnica.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.practicatecnica.entity.EstudianteEntity;

@Repository
public interface EstudianteRepository extends CrudRepository<EstudianteEntity, Long>{
	
	@Query(value = "SELECT * FROM estudiantes WHERE nombre = ?1", nativeQuery = true)
	public Optional<EstudianteEntity> findByName(String name);
	
	@Query(value = "Select * from estudiantes WHERE nombre like %:nombre%", nativeQuery = true)
	public List<EstudianteEntity> findByNombreContaining(String nombre);
	
}
