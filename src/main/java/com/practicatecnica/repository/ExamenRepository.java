package com.practicatecnica.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.practicatecnica.entity.ExamenEntity;

@Repository
public interface ExamenRepository extends CrudRepository<ExamenEntity, Long>{}
