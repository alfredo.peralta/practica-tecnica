package com.practicatecnica.repository;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.practicatecnica.entity.AsignacionEntity;

@Repository
public interface AsignacionRepository extends CrudRepository<AsignacionEntity, Long>{

	@Query("select a from AsignacionEntity a where a.estudiante.id in :id")
	public List<AsignacionEntity> findAsignacionesByStudent(Long id);

}
