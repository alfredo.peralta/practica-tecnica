package com.practicatecnica.entity;

import java.io.Serializable;
import java.util.List;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "examenes")
public class ExamenEntity implements Serializable{

	/**
	 * The constant serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String curso;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinTable(name = "tbl_examenes_opciones", joinColumns = @JoinColumn(name="id_examen")
	, inverseJoinColumns = @JoinColumn(name="id_opcion"))
	private List<OpcionesEntity> opcion;
	
	//@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "examen")
	//private AsignacionEntity asignacion;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<OpcionesEntity> getOpcion() {
		return opcion;
	}

	public void setOpcion(List<OpcionesEntity> opcion) {
		this.opcion = opcion;
	}
	
	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}
/*	
	public AsignacionEntity getAsignacion() {
		return asignacion;
	}

	public void setAsignacion(AsignacionEntity asignacion) {
		this.asignacion = asignacion;
	}*/
	
	@Override
	public String toString() {
		return "ExamenEntity [id=" + id + ", curso=" + curso + ", opcion=" + opcion + "]";
	}
	
}
