package com.practicatecnica.entity;

import java.io.Serializable;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

@Entity
@Table(name = "opciones")
public class OpcionesEntity implements Serializable{

	/*
	 * The constant serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String pregunta;
	private String opcionUno;
	private String opcionDos;
	private String opcionTres;
	private String opcionCuatro;
	private String correcta;
	private Double puntos;
	@Transient
	private String respuesta;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPregunta() {
		return pregunta;
	}

	public void setPregunta(String pregunta) {
		this.pregunta = pregunta;
	}

	public String getOpcionUno() {
		return opcionUno;
	}

	public void setOpcionUno(String opcionUno) {
		this.opcionUno = opcionUno;
	}

	public String getOpcionDos() {
		return opcionDos;
	}

	public void setOpcionDos(String opcionDos) {
		this.opcionDos = opcionDos;
	}

	public String getOpcionTres() {
		return opcionTres;
	}

	public void setOpcionTres(String opcionTres) {
		this.opcionTres = opcionTres;
	}

	public String getOpcionCuatro() {
		return opcionCuatro;
	}

	public void setOpcionCuatro(String opcionCuatro) {
		this.opcionCuatro = opcionCuatro;
	}

	public String getCorrecta() {
		return correcta;
	}

	public void setCorrecta(String correcta) {
		this.correcta = correcta;
	}
	
	public Double getPuntos() {
		return puntos;
	}

	public void setPuntos(Double puntos) {
		this.puntos = puntos;
	}
	
	public String getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	
}
