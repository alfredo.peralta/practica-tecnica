package com.practicatecnica.entity;

import java.io.Serializable;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "asignaciones")
public class AsignacionEntity implements Serializable{

	/**
	 * The constant serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String fechaHoraPresentacion;
	
	private String zonaHoraria;
	
	private Double calificacion;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private EstudianteEntity estudiante;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private ExamenEntity examen;
	
	public AsignacionEntity() {
		this.calificacion = 0.0;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

	public String getZonaHoraria() {
		return zonaHoraria;
	}

	public void setZonaHoraria(String zonaHoraria) {
		this.zonaHoraria = zonaHoraria;
	}

	public EstudianteEntity getEstudiante() {
		return estudiante;
	}

	public void setEstudiante(EstudianteEntity estudiante) {
		this.estudiante = estudiante;
	}

	public ExamenEntity getExamen() {
		return examen;
	}

	public void setExamen(ExamenEntity examen) {
		this.examen = examen;
	}
	
	public String getFechaHoraPresentacion() {
		return fechaHoraPresentacion;
	}

	public void setFechaHoraPresentacion(String fechaHoraPresentacion) {
		this.fechaHoraPresentacion = fechaHoraPresentacion;
	}
	
	public Double getCalificacion() {
		return calificacion;
	}
	
	public void setCalificacion(Double calificacion) {
		this.calificacion = calificacion;
	}
}
