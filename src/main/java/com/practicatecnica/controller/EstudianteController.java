package com.practicatecnica.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.practicatecnica.entity.EstudianteEntity;
import com.practicatecnica.service.EstudianteService;


@RestController
@RequestMapping("/estudiantes")
@Validated
public class EstudianteController {
	
	@Autowired
	private EstudianteService estudianteService;
	
	@PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> register(@RequestBody @Validated EstudianteEntity student){
		return estudianteService.register(student);
	}
	
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EstudianteEntity>> consultaEstudiantes(){
		return estudianteService.mostrarEstudiantes();
	}
	
	@GetMapping(value = "/{nombre}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<EstudianteEntity>> consultaEstudiantes(@PathVariable(required = true) String nombre){
		return estudianteService.mostrarEstudiantesPorNombre(nombre);
	}
	
	@PostMapping("/eliminar/{id}")
	public void eliminar(
			@PathVariable(required = true) Long id){
		estudianteService.eliminar(id);
	}
	
}
