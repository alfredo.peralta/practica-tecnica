package com.practicatecnica.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.practicatecnica.entity.AsignacionEntity;
import com.practicatecnica.service.RecopilarService;

@RestController
@RequestMapping("/recopilar")
public class RecopilarController {

	@Autowired
	private RecopilarService recopilarService;
	
	@PostMapping(value = "/enviar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> crear(@RequestBody @Validated AsignacionEntity asignacion){
		return recopilarService.recopilar(asignacion);
	}
	
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public AsignacionEntity crear(@PathVariable(required = true) Long id){
		return recopilarService.consultaPorId(id);
	}
	
}
