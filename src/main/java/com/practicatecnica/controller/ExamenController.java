package com.practicatecnica.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.practicatecnica.entity.ExamenEntity;
import com.practicatecnica.service.ExamenService;

@RestController
@RequestMapping("/examenes")
public class ExamenController {

	@Autowired
	private ExamenService examenService;
	
	@PostMapping(value = "/crear", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> register(@RequestBody @Validated ExamenEntity examen){
		return examenService.crearExamen(examen);
	}
	
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<ExamenEntity>> consultaExamenes(){
		return examenService.mostrarExamenes();
	}
	
	@PostMapping("/eliminar/{id}")
	public void eliminar(
			@PathVariable(required = true) Long id){
		examenService.eliminar(id);
	}
	
}
