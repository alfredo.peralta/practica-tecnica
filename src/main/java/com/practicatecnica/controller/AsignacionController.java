package com.practicatecnica.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.practicatecnica.dto.FechaHora;
import com.practicatecnica.entity.AsignacionEntity;
import com.practicatecnica.service.AsignacionService;

@RestController
@RequestMapping("/asignaciones")
public class AsignacionController {

	@Autowired
	private AsignacionService asignacionService; 
	
	@PostMapping(value = "/crear/{idExamen}/{idEstudiante}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> crear(@RequestBody @Validated FechaHora fecha, 
			@PathVariable(required = true) Long idExamen, 
			@PathVariable(required = true) Long idEstudiante){
		return asignacionService.asignar(fecha, idExamen, idEstudiante);
	}
	
	@PostMapping(value = "/modificar/{idAsignacion}/{idExamen}/{idEstudiante}", produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> modificar(@RequestBody @Validated FechaHora fecha, 
			@PathVariable(required = true) Long idAsignacion,
			@PathVariable(required = true) Long idExamen, 
			@PathVariable(required = true) Long idEstudiante){
		return asignacionService.actualizar(fecha, idAsignacion, idExamen, idEstudiante);
	}
	
	@GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AsignacionEntity> consultaAsignaciones(){
		return asignacionService.asignaciones();
	}
	
	@GetMapping(value = "/buscar/{idEstudiante}", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<AsignacionEntity> consultaAsignaciones(@PathVariable(required = true) Long idEstudiante){
		return asignacionService.buscarAsignacion(idEstudiante);
	}
	
	@PostMapping("/eliminar/{id}")
	public void eliminar(
			@PathVariable(required = true) Long id){
		asignacionService.eliminar(id);
	}
	
}
