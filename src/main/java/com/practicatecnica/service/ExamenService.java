package com.practicatecnica.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.practicatecnica.entity.ExamenEntity;
import com.practicatecnica.repository.ExamenRepository;

@Service
public class ExamenService {
	
	@Autowired
	private ExamenRepository examenRepository;
	
	public ResponseEntity<List<ExamenEntity>> mostrarExamenes(){
		return ResponseEntity.ok().body((List<ExamenEntity>) examenRepository.findAll());
	}
	
	public ResponseEntity<Object> crearExamen(ExamenEntity examen){
		
		if (examen.getId()!=null) {
		
		/** Se consulta si existe el examen **/
		Optional<ExamenEntity> exam =  examenRepository.findById(examen.getId());
		
		/** Se declara respuesta **/
		if (exam.isPresent()) {
			examen.setId(exam.get().getId());
		   }
		}
		examen = examenRepository.save(examen);
		return ResponseEntity.ok().body(examen);
	}
	
	public void eliminar(Long id) {
		Optional<ExamenEntity> examen = examenRepository.findById(id);
		if (examen.isPresent()) {
			examenRepository.delete(examen.get());
		}
	}

}
