package com.practicatecnica.service;


import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.practicatecnica.entity.AsignacionEntity;
import com.practicatecnica.entity.EstudianteEntity;
import com.practicatecnica.entity.ExamenEntity;
import com.practicatecnica.entity.OpcionesEntity;
import com.practicatecnica.repository.AsignacionRepository;
import com.practicatecnica.repository.EstudianteRepository;
import com.practicatecnica.repository.ExamenRepository;

@Service
public class RecopilarService {

	@Autowired
	private AsignacionRepository asignacionRepository;
	
	@Autowired
	private EstudianteRepository estudianteRepository; 
	
	@Autowired
	private ExamenRepository examenRepository;
	
	public ResponseEntity<Object> recopilar(AsignacionEntity asignacion) {
		
		Optional<EstudianteEntity> estudiante = estudianteRepository.findById(asignacion.getEstudiante().getId());
		Optional<ExamenEntity> examen = examenRepository.findById(asignacion.getExamen().getId());
		
		if (estudiante.isPresent() && examen.isPresent()) {
			/** Se realiza calificacion **/
			List<OpcionesEntity> c = calificar(asignacion.getExamen().getOpcion());
			Double cf = calificacion(c);
			
			asignacion.setCalificacion(cf);
			asignacionRepository.save(asignacion);
			return ResponseEntity.ok().body(asignacion);
		}
		
		return ResponseEntity.badRequest().body(Collections.singletonMap("message", "No hay estudiante o examen."));
	}
	
	public AsignacionEntity consultaPorId(Long idAsignacion){
		Optional<AsignacionEntity> asignacion = asignacionRepository.findById(idAsignacion);
		Optional<EstudianteEntity> estudiante = estudianteRepository.findById(asignacion.get().getEstudiante().getId());
		Optional<ExamenEntity> examen = examenRepository.findById(asignacion.get().getExamen().getId());
		asignacion.get().setExamen(examen.get());
		asignacion.get().setEstudiante(estudiante.get());
		return asignacion.get();
	}
	
	private List<OpcionesEntity> calificar(List<OpcionesEntity> opciones) {
		for(int i = 0; i < opciones.size(); i++) {
			if(opciones.get(i).getCorrecta().equals(opciones.get(i).getRespuesta())) {
				opciones.get(i).setPuntos(20.0);
			}
		}
		return opciones;
	}
	
	private Double calificacion(List<OpcionesEntity> opciones) {
		Double calificacionFinal = 0.0;
		for(int i = 0; i < opciones.size(); i++) {
			calificacionFinal = calificacionFinal + opciones.get(i).getPuntos();
		}
		return calificacionFinal;
	}
	
}
