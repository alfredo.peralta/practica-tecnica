package com.practicatecnica.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.practicatecnica.dto.FechaHora;
import com.practicatecnica.entity.AsignacionEntity;
import com.practicatecnica.entity.EstudianteEntity;
import com.practicatecnica.entity.ExamenEntity;
import com.practicatecnica.repository.AsignacionRepository;
import com.practicatecnica.repository.EstudianteRepository;
import com.practicatecnica.repository.ExamenRepository;

@Service
public class AsignacionService {

	@Autowired
	private AsignacionRepository asignacionRepository;
	
	@Autowired
	private EstudianteRepository estudianteRepository; 
	
	@Autowired
	private ExamenRepository examenRepository;
	
	public ResponseEntity<Object> asignar(FechaHora fecha, Long idExamen, Long idEstudiante) {
		
		Optional<EstudianteEntity> estudiante = estudianteRepository.findById(idEstudiante);
		Optional<ExamenEntity> examen = examenRepository.findById(idExamen);
		AsignacionEntity asignacion = new AsignacionEntity();
		
		if (estudiante.isPresent() && examen.isPresent()) {
			/** ZonaHoraria **/
			LocalDate ld = LocalDate.parse(fecha.getFecha());
			LocalTime lt = LocalTime.parse(fecha.getHora());
			LocalDateTime ldt = LocalDateTime.of(ld, lt);
			ZonedDateTime fechaExamen = ZonedDateTime.of(ldt, ZoneId.of("America/Bogota"));
			asignacion.setFechaHoraPresentacion(fechaExamen.toString());
			ZonedDateTime zonaEstudiante = ZonedDateTime.of(ldt, ZoneId.of(estudiante.get().getZonaHoraria()));
			asignacion.setZonaHoraria(zonaEstudiante.toString());
			/** Se realiza asignacion **/
			asignacion.setEstudiante(estudiante.get());
			asignacion.setExamen(examen.get());
			/** Se realiza respuesta **/
			asignacion = asignacionRepository.save(asignacion);
			return ResponseEntity.ok().body(asignacion);
		}
		
		return ResponseEntity.badRequest().body(Collections.singletonMap("message", "No hay estudiante o examen."));
	}
	
	public ResponseEntity<Object> actualizar(FechaHora fecha, Long idAsignacion, Long idExamen, Long idEstudiante) {
		Optional<AsignacionEntity> uAsignacion = asignacionRepository.findById(idAsignacion);
		
		if (uAsignacion.isPresent()) {
			AsignacionEntity asignacion = new AsignacionEntity();
			Optional<EstudianteEntity> estudiante = estudianteRepository.findById(idEstudiante);
			Optional<ExamenEntity> examen = examenRepository.findById(idExamen);
			asignacion = uAsignacion.get();
			/** ZonaHoraria **/
			LocalDate ld = LocalDate.parse(fecha.getFecha());
			LocalTime lt = LocalTime.parse(fecha.getHora());
			LocalDateTime ldt = LocalDateTime.of(ld, lt);
			ZonedDateTime fechaExamen = ZonedDateTime.of(ldt, ZoneId.of("America/Bogota"));
			asignacion.setFechaHoraPresentacion(fechaExamen.toString());
			ZonedDateTime zonaEstudiante = ZonedDateTime.of(ldt, ZoneId.of(asignacion.getEstudiante().getZonaHoraria()));
			asignacion.setZonaHoraria(zonaEstudiante.toString());
			/** Se realiza respuesta **/
			asignacion.setEstudiante(estudiante.get());
			asignacion.setExamen(examen.get());
			asignacion = asignacionRepository.save(asignacion);
			return ResponseEntity.ok().body(asignacion);
		}
		
		return ResponseEntity.badRequest().body(Collections.singletonMap("message", "No se encuentra la asignación."));
	}
	
	public void eliminar(Long id) {
		Optional<AsignacionEntity> asignacion = asignacionRepository.findById(id);
		if (asignacion.isPresent()) {
			asignacion.get().setEstudiante(null);
			asignacion.get().setExamen(null);
			asignacionRepository.deleteById(id);
		}
	}
	
	public List<AsignacionEntity> asignaciones(){
		return (List<AsignacionEntity>) asignacionRepository.findAll();
	}
	
	public List<AsignacionEntity> buscarAsignacion(Long id){
		return asignacionRepository.findAsignacionesByStudent(id);
	}
	
}
