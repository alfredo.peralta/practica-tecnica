package com.practicatecnica.service;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.practicatecnica.entity.EstudianteEntity;
import com.practicatecnica.repository.EstudianteRepository;
import jakarta.transaction.Transactional;

@Service
public class EstudianteService {
	
	@Autowired
	private EstudianteRepository estudianteRepository; 
	
	@Transactional
	public ResponseEntity<List<EstudianteEntity>> mostrarEstudiantes() {
		return ResponseEntity.ok().body((List<EstudianteEntity>)estudianteRepository.findAll());
	}
	
	@Transactional
	public ResponseEntity<Object> register(EstudianteEntity estudiante) {
		/** Se consulta si existe el estudiante **/
		
		if (estudiante.getId()!=null) {
		Optional<EstudianteEntity> student = estudianteRepository.findById(estudiante.getId());; 
		
		/** Se declara respuesta **/
		if (student.isPresent()) {
			student.get().setCiudad(estudiante.getCiudad());
			student.get().setEdad(estudiante.getEdad());
			student.get().setNombre(estudiante.getNombre());
			student.get().setZonaHoraria(estudiante.getZonaHoraria());
			estudiante = student.get();
		   }
		}
		estudiante = estudianteRepository.save(estudiante);
		return ResponseEntity.ok().body(estudiante);
	}

	public void eliminar(Long id) {
		Optional<EstudianteEntity> estudiante = estudianteRepository.findById(id);
		if (estudiante.isPresent()) {
			estudianteRepository.delete(estudiante.get());
		}
	}
	
	public ResponseEntity<List<EstudianteEntity>> mostrarEstudiantesPorNombre(String nombre) {
		/** Se consulta si existe el estudiante **/
		List<EstudianteEntity> student =  estudianteRepository.findByNombreContaining(nombre);
		/** Se declara respuesta **/
		return ResponseEntity.badRequest().body(student);
	}

}
