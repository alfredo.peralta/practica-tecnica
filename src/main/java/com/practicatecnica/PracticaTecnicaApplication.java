package com.practicatecnica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticaTecnicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticaTecnicaApplication.class, args);
	}

}
